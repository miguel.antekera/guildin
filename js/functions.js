// Detect mobile
var isMobile = {
  Android: function() {
    return navigator.userAgent.match(/Android/i);
  },
  BlackBerry: function() {
    return navigator.userAgent.match(/BlackBerry/i);
  },
  iOS: function() {
    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },
  Opera: function() {
    return navigator.userAgent.match(/Opera Mini/i);
  },
  Windows: function() {
    return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
  },
  any: function() {
    return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
  }
};

// Send mail functions
function sendMail() {
  var form = $('#contactForm');
  var formMessages = $('#form-messages');
  var formData = $(form).serialize();
  $.ajax({
      type: 'POST',
      async: true,
      url: $(form).attr('action'),
      data: formData
    })
    .done(function(response) {
      $(formMessages).removeClass('alert-danger');
      $(formMessages).addClass('alert-success');
      $(formMessages).text(response);
      $('#name').val('').removeClass('valid');
      $('#email').val('').removeClass('valid');
      $('#phone').val('').removeClass('valid');
      $('#message').val('').removeClass('valid');
      console.log("enviado");
    })
    .fail(function(data) {
      $(formMessages).removeClass('alert-success');
      $(formMessages).addClass('alert-danger');
      if (data.responseText !== '') {
        $(formMessages).text(data.responseText);
      } else {
        $(formMessages).text('Oops! your message could not be sent, please try again.');
      }
      console.log("fallo");
    });
}

// Ease scroll function
function easeScroll(size) {
  $('.nav li a[href^="#"], .logo a[href^="#"], .ease[href^="#"]').on('click', function(e) {
    e.preventDefault();
    var target = this.hash;
    var $target = $(target);
    $('html, body').stop().animate({
      'scrollTop': $target.offset().top - size
    }, 900, 'swing', function() {
      // window.location.hash = target;
    });
    $('.navbar-toggle').click();
    return false;
  });
}

// Sections height
var brandHeight = $("#about").height();
var aboutHeight = $("#about").height();
var whyUsHeight = $("#why-us").height() + $("#steps").height();
var servicesHeight = $("#services").height() + $(".when").height();
var teamHeight = $("#team").height();
var contactHeight = $("#contact").height();

// Init scroll magic
var controller = new ScrollMagic.Controller();

// Active buttons on scroll function
function scrollScene(section, height) {
  new ScrollMagic.Scene({
      triggerElement: section,
      duration: height,
      reverse: false
    })
    .on('start', function () {
        // console.log("passed trigger"+section);
    })
    // .setClass("outline")
    .setClassToggle("[href^='" + section + "'", "active")
    // .addIndicators() // add indicators (requires plugin)
    .addTo(controller);
}

// Sticky class header
function stickyClass() {
  new ScrollMagic.Scene({
      triggerElement: ".brand",
    })
    .setClassToggle("body", "sticky")
    // .addIndicators() // add indicators (requires plugin)
    .addTo(controller);
}

scrollScene('#about', aboutHeight);
scrollScene('#why-us', whyUsHeight);
scrollScene('#services', servicesHeight);
scrollScene('#team', teamHeight);
scrollScene('#contact', contactHeight);

stickyClass();

// Brand animations

function splitme(text) {
  var showText = function () {
    $(text).removeClass('invisible');
  }
  var tl =  new TimelineMax();
  mySplitText = new SplitText(text, {type:"words,chars"}),
  chars = mySplitText.chars;
  tl.staggerFrom(chars,0.01, {opacity:0, onComplete: showText, ease:Power1.easeIn}, 0.03, "+=0.1");
}

function aniBrand() {
  var scene = new ScrollMagic.Scene({triggerElement: ".brand", reverse: false, offset: -100})
    .on('start', function () {
      splitme(".brand .section-title");
      $('.brand .invisible').removeClass('invisible');
    })
    .setTween(TweenMax.to(".cofee", 1, {opacity: "1", ease: Linear.easeNone, delay:1}))
    // .addIndicators({name: "pin scene", colorEnd: "#FFFFFF"})
    .addTo(controller);
}

// About animations

function aniAbout() {
  var scene = new ScrollMagic.Scene({triggerElement: ".about", reverse: false, offset: -100})
  .on('start', function () {

    $('.about .invisible').removeClass('invisible');

    var preloaderOutTl = new TimelineMax();
    preloaderOutTl

    .addLabel('targetPoint')

    preloaderOutTl.add( TweenLite.fromTo('.about .h1', 1, { y:-40, opacity:0 }, { y:0, opacity:1, delay: 0.4 }) , 'targetPoint+=0.4');
    preloaderOutTl.add( TweenLite.fromTo('.about .g-logo', 1, { y:-40, opacity:0 }, { y:0, opacity:1, delay: 0.4 }) , 'targetPoint');
    preloaderOutTl.add( TweenLite.fromTo('.about .h5', 1, { y:-20, opacity:0 }, { y:0, opacity:1 }) , 'targetPoint+=1.6');

    preloaderOutTl.add( TweenLite.fromTo('.about .lamp', 1, { y:-200 }, { y:0 }) , 'targetPoint+=1.4');
    preloaderOutTl.add( TweenLite.fromTo('.about .bright', 0.6, { opacity:0 }, { opacity:1 }) , 'targetPoint+=2.4');
    preloaderOutTl.add( TweenLite.fromTo('.about .light', 0.6, { opacity:0 }, { opacity:1 }) , 'targetPoint+=2.4');
    preloaderOutTl.add( TweenLite.fromTo('.about .tree', 0.6, { scale:0 }, { scale:1 }) , 'targetPoint+=3');

    preloaderOutTl.add( TweenMax.to('.why-us .cubo-bg', 15, { rotation: 360, repeat: -1, ease: Power0.easeNone, delay: 0.4 }) , 'targetPoint+=0.4');

    return preloaderOutTl

  })

  .addTo(controller);
}

// Why us animations

function aniWhy() {
  var scene = new ScrollMagic.Scene({triggerElement: ".why-us", reverse: false, offset: -100})
  .on('start', function () {

    $('.why-us .invisible').removeClass('invisible');

    var preloaderOutTl = new TimelineMax();
    preloaderOutTl

    .addLabel('targetPoint')

    preloaderOutTl.add( TweenMax.fromTo('.why-us .cubo', 1, { rotation: -180, opacity:0, scale:4, left: -500 }, { ease:  Back.easeOut, left:0, scale:1, rotation:0, opacity:1, delay: 0.4 }) , 'targetPoint');
    preloaderOutTl.add( TweenLite.fromTo('.why-us .h1', 1, { y:-40, opacity:0 }, { y:0, opacity:1, delay: 0.4 }) , 'targetPoint+=0.8');
    preloaderOutTl.add( TweenLite.fromTo('.why-us .h5', 1, { y:-40, opacity:0 }, { y:0, opacity:1, delay: 0.4 }) , 'targetPoint+=1.2');

    return preloaderOutTl

  })
  // .addIndicators({name: "trigger why", colorEnd: "#FFFFFF"})
  .addTo(controller);
}

// Steps animation
function aniCompu() {
  var scene = new ScrollMagic.Scene({triggerElement: ".stagger-elements-warning", reverse: false, offset: -20})
  .on('start', function () {

    $('.stagger-elements-warning .invisible').removeClass('invisible');

    var preloaderOutTl = new TimelineMax();
    preloaderOutTl

    .addLabel('targetPoint')

    TweenMax.staggerFrom(".stagger-elements-warning .stagger", 1, {opacity:0, y:200, ease:Back.easeOut, delay:.4}, 0.2);

    return preloaderOutTl

  })
  // .addIndicators({name: "trigger stagger-elements-warning", colorEnd: "#FFFFFF"})
  .addTo(controller);
}

// services animation
function aniServices() {
  var scene = new ScrollMagic.Scene({triggerElement: ".stagger-elements-services", reverse: false, offset: -20})
  .on('start', function () {

    $('.stagger-elements-services .invisible').removeClass('invisible');

    var preloaderOutTl = new TimelineMax();
    preloaderOutTl

    .addLabel('targetPoint')

    TweenMax.staggerFrom(".stagger-elements-services .stagger", 1, {opacity:0, y:200, ease:Back.easeOut, delay:.4}, 0.2);

    return preloaderOutTl

  })
  // .addIndicators({name: "trigger stagger-elements-services", colorEnd: "#FFFFFF"})
  .addTo(controller);
}

// contact
function aniContact() {
  var scene = new ScrollMagic.Scene({triggerElement: ".stagger-elements-normal", reverse: false, offset: -100})
  .on('start', function () {

    $('.stagger-elements-normal .invisible').removeClass('invisible');

    var preloaderOutTl = new TimelineMax();
    preloaderOutTl

    .addLabel('targetPoint')

    TweenMax.staggerFrom(".stagger-elements-normal .stagger", 1, {opacity:0, y:200, ease:Power1.easeOut, delay:.4}, 0.2);

    return preloaderOutTl

  })
  // .addIndicators({name: "trigger stagger-elements-normal", colorEnd: "#FFFFFF"})
  .addTo(controller);
}

// team
function aniTeam() {
  var scene = new ScrollMagic.Scene({triggerElement: ".team", reverse: false, offset: -20})
  .on('start', function () {

    $('.team .invisible').removeClass('invisible');

    var preloaderOutTl = new TimelineMax();
    preloaderOutTl

    .addLabel('targetPoint')

    TweenMax.staggerFrom(".team .stagger", 1, {opacity:0, y:100, ease:Power1.easeOut, delay:.4}, 0.2);

    preloaderOutTl.add( TweenLite.fromTo('.team .telescope-wrapper', 0.6, { opacity:0 }, { opacity:1, delay: 1 }));

    return preloaderOutTl

  })
  // .addIndicators({name: "trigger team", colorEnd: "#FFFFFF"})
  .addTo(controller);
}

// steps
function aniSteps() {
  var scene = new ScrollMagic.Scene({triggerElement: ".stagger-elements-steps", reverse: false, offset: -20})
  .on('start', function () {

    $('.stagger-elements-steps .invisible').removeClass('invisible');

    var preloaderOutTl = new TimelineMax();
    preloaderOutTl

    .addLabel('targetPoint')

    TweenMax.staggerFrom(".stagger-elements-steps .stagger", 1, {opacity:0, y:100, ease:Power1.easeOut, delay:.4}, 0.2);

    preloaderOutTl.add( TweenLite.fromTo('.stagger-elements-steps .telescope-wrapper', 0.6, { opacity:0 }, { opacity:1, delay: 1 }));

    return preloaderOutTl

  })
  // .addIndicators({name: "trigger team", colorEnd: "#FFFFFF"})
  .addTo(controller);
}


// When animation
function aniWhen() {
  var scene = new ScrollMagic.Scene({triggerElement: ".when", reverse: false, offset: -100})
    .on('start', function () {
      splitme(".when .section-title");
    })
    // .addIndicators({name: "pin scene", colorEnd: "#FFFFFF"})
    .addTo(controller);
}

// Each animation
function eachWhen() {
  var scene = new ScrollMagic.Scene({triggerElement: ".each", reverse: false, offset: -100})
    .on('start', function () {
      splitme(".each .section-title");
    })
    // .addIndicators({name: "pin scene", colorEnd: "#FFFFFF"})
    .addTo(controller);
}

// About animations

function aniSections() {
  aniBrand();
  aniAbout();
  aniWhy();
  aniWhen();
  eachWhen();
  aniCompu();
  aniSteps();
  aniContact();
  aniTeam();
  aniServices();
}



$(document).ready(function() {

	// Image loader

	// number of loaded images for preloader progress
	var loadedCount = 0; //current number of images loaded
	var imagesToLoad = $('img').length; //number of slides with .bcg container
	var loadingProgress = 0; //timeline progress - starts at 0

	$('body').imagesLoaded()
  .always( function( instance ) {
    // console.log('all images loaded');
  })
  .done( function( instance ) {
    // console.log('all images successfully loaded');
  })
  .fail( function() {
    // console.log('all images loaded, at least one is broken');
  })
  .progress( function( instance, image ) {
  	loadProgress();
    // var result = image.isLoaded ? 'loaded' : 'broken';
    // console.log( 'image is ' + result + ' for ' + image.img.src );
  });

  function loadProgress(imgLoad, image) {
	 	//one more image has been loaded
		loadedCount++;
		loadingProgress = (loadedCount/imagesToLoad);
		TweenLite.to(progressTl, 0.7, {progress:loadingProgress, ease:Linear.easeNone});
	}

	//progress animation instance. the instance's time is irrelevant, can be anything but 0 to void  immediate render
	var progressTl = new TimelineMax({paused:true,onUpdate:progressUpdate,onComplete:loadComplete});

	progressTl
	//tween the progress bar width
	.to($('.progress span'), 1, {width:100, ease:Linear.easeNone});

	//as the progress bar witdh updates and grows we put the precentage loaded in the screen
	function progressUpdate() {
		//the percentage loaded based on the tween's progress
		loadingProgress = Math.round(progressTl.progress() * 100);
		//we put the percentage in the screen
		$(".txt-perc").text(loadingProgress + '%');
	}



	function loadComplete() {

    // function apagalaluz() {
      $('.neon-wrapper').addClass('turn-on');

      function apagalaluz() {
        // var min = 3, max = 7;
        // var rand = Math.floor(Math.random() * (max - min + 1) + min);


        // $('.cover').addClass('apagado');

        setInterval(function(){

          $('.cover').addClass('apagado');
          // setTimeout(apagalaluz, rand * 1000);
          setTimeout(function(){
             $('.cover').removeClass('apagado');
          },100);
           // console.log(rand);
        },3000);

        setInterval(function(){

          $('.cover').addClass('apagado');
          // setTimeout(apagalaluz, rand * 1000);
          setTimeout(function(){
             $('.cover').removeClass('apagado');
          },1000);
           // console.log(rand);
        },5000);

        // setTimeout(apagalaluz, rand * 1000);
      }

      // apagalaluz();

      // setInterval(function(){
      //  $('.cover').addClass('apagado');
      //    setTimeout(function(){
      //      $('.cover').removeClass('apagado');
      //    },2000);
      //   },5000);
      // }

    $('.nav li').removeClass('invisible');
    $('.cover .invisible').removeClass('invisible');

    var preloaderOutTl = new TimelineMax({onComplete:apagalaluz()});

		preloaderOutTl

			.addLabel('targetPoint')

      .to($('.progress'), 0.3, {y: 100, autoAlpha: 0, ease:Back.easeIn})
			.to($('.txt-perc'), 0.3, {y: 100, autoAlpha: 0, ease:Back.easeIn}, 0.1)
			.set($('body'), {className: '-=is-loading'})
			.to($('#preloader'), 1, {yPercent: 100, ease:Power4.easeInOut}, 'targetPoint')
			.set($('#preloader'), {className: '+=is-hidden'})

      .fromTo($('.header .logo'), 1, {autoAlpha:0, y: -30}, {autoAlpha:1, y: 0 }, 'targetPoint')
      .staggerFromTo($('.navbar li'), 1, {autoAlpha:0}, {autoAlpha:1, ease:Power1.easeOut, delay:.4}, 0.2, 'targetPoint+=0')

      .to($('.neon-wrapper'), 1, {className: '+=is-loaded'}, 'targetPoint')
      // .set($('.neon-wrapper'), {className: '+=turn-on'}, 'targetPoint+=1.4')

			.from($('.cover .title'), 0.5, {autoAlpha: 0, ease:Power1.easeOut}, 'targetPoint+=1.4')
			.from($('.cover .text'), 0.5, {autoAlpha: 0, ease:Power1.easeOut}, 'targetPoint+=1.6')
			.from($('.scroll-hint'), 1, {y: -20, autoAlpha: 0, ease:Power1.easeOut, repeat: -1}, 'targetPoint+=1.8')

		return preloaderOutTl;
	}

  $('.steps .stagger').on('mouseover touchstart', function() {
    $(this).find('.number').addClass('active');
    $(this).siblings().find('.number').removeClass('active');
  });

  // Mobile device functions
  function mobile(e) {
    // console.log("soy mobile");
    // $("body .invisible").removeClass('invisible');
    aniSections();
  }

  // Not mobile device functions
  function not_mobile(e) {
    $('body').addClass('desktop');
    // console.log("no soy mobile");
    aniSections();
  }

  // Small device functions
  function smallDevice(e) {
    // console.log("tablet");
    $('.content').addClass('container-fluid');
    $('.content').removeClass('container');
    easeScroll(72);

    $('.nav a, .logo a').on('click', function(){
        $('.navbar-toggler').trigger('click');
    });
  }

  // Large device functions
  function largeDevice(e) {
    // console.log("desktop");
    $('.content').addClass('container');
    $('.content').removeClass('container-fluid');
    easeScroll(49);
  }

  // Carousel team
  var titleMain = $("#animatedHeading");
  var titleSubs = titleMain.find("slick-active");
  if (titleMain.length) {
    titleMain.slick({
      autoplay: false,
      arrows: false,
      dots: false,
      slidesToShow: 3,
      centerPadding: "10px",
      draggable: false,
      infinite: true,
      pauseOnHover: false,
      swipe: false,
      touchMove: false,
      vertical: true,
      speed: 1000,
      autoplaySpeed: 2000,
      useTransform: true,
      cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
      adaptiveHeight: true,
    });

    $(".slick-dupe").each(function(index, el) {
      $("#animatedHeading").slick('slickAdd', "<div>" + el.innerHTML + "</div>");
    });

    titleMain.slick('slickPlay');
  };

  // Form validation
  $.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i.test(value);
  }, "Letters only.");

  $.validator.addMethod("regexNumber", function(value, element) {
    return this.optional(element) || /^[+-]?\d+$/i.test(value);
  }, "Number only.");

  $(".validate-form").validate({
    ignore: [],
    // errorPlacement: function(error, element) {
    //   error.appendTo(element.parents(".js-form-wrapper"));
    // },
    rules: {
      name: {
        required: true,
        lettersonly: true,
        maxlength: 100,
      },
      phone: {
        required: true,
        regexNumber: true,
      },
      email: {
        required: true,
        email: true,
        maxlength: 60,
      },
      message: {
        required: true,
        maxlength: 1000,
      },
      // "hiddenRecaptcha": {
      //   required: function() {
      //     if(grecaptcha.getResponse() == '') {
      //         return true;
      //     } else {
      //         return false;
      //     }
      //   }
      // }
    },
    messages: {
      name: {
        required: "Required field.",
        maxlength: "Max length exceed.",
      },
      phone: {
        required: "Required field.",
        number: "Only numbers.",
      },
      email: {
        required: "Required field.",
        email: "Invalid email.",
        maxlength: "Max length exceed.",
      },
      pGlosa: {
        required: "Required field.",
        maxlength: "Max length exceed.",
      },
      // hiddenRecaptcha: "Debes validar este campo para continuar.",
    },
    submitHandler: function() {
      console.log("enviado");
      sendMail();
    }
  });

  // To mobile devices
  if (isMobile.any()) mobile();
  else not_mobile();

  // To media queries
  (function(window, document, undefined) {
    'use strict';
    var mediaQuery = window.matchMedia('(max-width: 767px)');
    mediaQuery.addListener(doSomething);
    function doSomething(mediaQuery) {
      if (mediaQuery.matches) {
        smallDevice();
      } else {
        largeDevice();
      }
    }
    doSomething(mediaQuery);
  })(window, document);

});
